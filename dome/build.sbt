name := "dome"
version := "0.1"
scalaVersion := "2.12.10"

val aetherHome = sys.env("AETHER_HOME")
lazy val platform = RootProject(file(s"$aetherHome/projects/lwjgl"))
lazy val lib = RootProject(file("../aether/projects/lib"))
lazy val apps = (project in file(".")).dependsOn(platform,lib)

unmanagedSourceDirectories in Compile := Seq(
  baseDirectory.value / "src",
)

