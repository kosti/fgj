package dome

import net.eelemental.aether.sys.Application
import net.eelemental.aether.sys.Display
import net.eelemental.aether.sys.Event
import net.eelemental.aether.sys.Renderer
import net.eelemental.aether.sys.Texture
import scala.util.Random
import net.eelemental.aether.types.Vec2F
import net.eelemental.aether.util.MathF
import net.eelemental.aether.lib.shader.Canvas
import net.eelemental.aether.sys.ShaderProgram
import net.eelemental.aether.lib.model.MeshPT
import net.eelemental.aether.lib.model.Mesh
import net.eelemental.aether.sys.Ref
import net.eelemental.aether.sys.Sys
import net.eelemental.aether.lib.about.Logo
import net.eelemental.aether.sys.KeyEvent

class Dome extends Application {

  //    Display.create(Display.Config(sizeX = 1400, sizeY = 1050, fullscreen = true))
  //    Display.create(Display.Config(sizeX = 1024, sizeY = 1024, fullscreen = true))

  Display(Display.Config(sizeX = 4096, sizeY = 1080, fullscreen = true))
  //  Display.create(Display.Config(sizeX = 512, sizeY = 512, fullscreen = false))

  //  Display.create(Display.Config(sizeX = 2048, sizeY = 2160, fullscreen = true))
  //          Display.create(Display.Config(sizeX = 512, sizeY = 512, fullscreen = false))
  //        Display.create(Display.Config(sizeX = 2048, sizeY = 1080, fullscreen = false))
  //      Display.create(Display.Config(sizeX = 1080, sizeY = 1080, fullscreen = false))
  //  Display.create(Display.Config(sizeX = 1080, sizeY = 1080, fullscreen = false))

  //  if (Config.enableProjection) {
  //  } else {
  //    Display.create(Display.Config(sizeX = 1024, sizeY = 1024, fullscreen = false))
  //  }

  var state: GameState = ShaderToy

  def init() {
    initConfig()
  }

  def uninit() {
  }

  def event(event: Event): Unit = {
    event match {
      case KeyEvent(Event.Type.KeyPress, keyCode, _, _) =>
        keyCode match {
          case Event.KeyCode.ESCAPE =>
            Sys.exit()
          case Event.KeyCode.F1 => state = Projector
          case Event.KeyCode.F2 => state = Jams
          case Event.KeyCode.F3 => state = Firefly
          case Event.KeyCode.F4 => state = Quantum
          case Event.KeyCode.F5 => state = Swirl
          case Event.KeyCode.F6 => state = ShaderToy
          case Event.KeyCode.S  => Projection.write()
          case Event.KeyCode.R  => Projection.createFisheye()
          case Event.KeyCode.P  => Global.enableProjection = !Global.enableProjection
          case Event.KeyCode.M  => Global.paintMarkers = !Global.paintMarkers
          case Event.KeyCode.U  => Global.uniformScale = !Global.uniformScale
          case Event.KeyCode.N  => Global.projectionMap = (Global.projectionMap + 1) % Global.projectionMapCount
          case _                =>
        }
      case _ =>
    }
    state.event(event)
  }

  def update() {
    if (Global.configSheet.changed) initConfig()

    state.update()
  }

  def paint() {
    state.paint()

  }

  def initConfig() {
    Global.configSheet.load()
    Projection.createMapping()
  }

}