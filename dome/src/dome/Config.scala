package dome

import net.eelemental.aether.lib.sheets.ValueIterator
import net.eelemental.aether.lib.sheets.IntValue
import net.eelemental.aether.lib.sheets.FloatValue
import net.eelemental.aether.lib.sheets.BooleanValue
import net.eelemental.aether.lib.sheets.StringValue

object Config {

  val displaySizeX = new IntValue("display.size.x")
  val displaySizeY = new IntValue("display.size.y")

  val mappingSizeX = new IntValue("mappingSizeX")
  val mappingSizeY = new IntValue("mappingSizeY")

  val projectionFile = new StringValue("projection.file")
  val projectionCombined = new BooleanValue("projection.combined")
  val projectionVertical = new BooleanValue("projection.vertical")
  val projectionZigzag = new BooleanValue("projection.zigzag")

  val pitchMultiplier = new FloatValue("pitch.multiplier")

  object Sphere {
    val offsetX = new FloatValue("sphere.offsetX")
    val offsetY = new FloatValue("sphere.offsetY")
    val scaleX = new FloatValue("sphere.scaleX")
    val scaleY = new FloatValue("sphere.scaleY")
  }

  object Dome1 {
    val projectorY = new FloatValue("dome1.projectorY")
    val projectorZ = new FloatValue("dome1.projectorZ")
    val tilt = new FloatValue("dome1.tilt")
    val skew = new FloatValue("dome1.skew")
    val rotate = new FloatValue("dome1.rotate")
    val topX = new FloatValue("dome1.topX")
    val topY = new FloatValue("dome1.topY")
    val bottomY = new FloatValue("dome1.bottomY")
    val leftX = new FloatValue("dome1.leftX")
    val rightX = new FloatValue("dome1.rightX")
    val sideY = new FloatValue("dome1.sideY")
  }

  object Dome2 {
    val projectorY = new FloatValue("dome2.projectorY")
    val projectorZ = new FloatValue("dome2.projectorZ")
    val tilt = new FloatValue("dome2.tilt")
    val skew = new FloatValue("dome2.skew")
    val rotate = new FloatValue("dome2.rotate")
    val topX = new FloatValue("dome2.topX")
    val topY = new FloatValue("dome2.topY")
    val bottomY = new FloatValue("dome2.bottomY")
    val leftX = new FloatValue("dome2.leftX")
    val rightX = new FloatValue("dome2.rightX")
    val sideY = new FloatValue("dome2.sideY")
  }

  val values = Seq(
    displaySizeX, displaySizeY, mappingSizeX, mappingSizeY,
    projectionFile, projectionCombined, projectionVertical, projectionZigzag,
    pitchMultiplier,
    Sphere.offsetX, Sphere.offsetY, Sphere.scaleX, Sphere.scaleY,
    Dome1.projectorY, Dome1.projectorZ, Dome1.tilt, Dome1.skew, Dome1.rotate, Dome1.topX, Dome1.topY, Dome1.bottomY, Dome1.leftX, Dome1.rightX, Dome1.sideY,
    Dome2.projectorY, Dome2.projectorZ, Dome2.tilt, Dome2.skew, Dome2.rotate, Dome2.topX, Dome2.topY, Dome2.bottomY, Dome2.leftX, Dome2.rightX, Dome2.sideY)

}

