package dome

import net.eelemental.aether.lib.util.Colors
import net.eelemental.aether.lib.util.Textures
import net.eelemental.aether.util.MathF
import net.eelemental.aether.util.MathF._

import net.eelemental.aether.sys.Renderer
import net.eelemental.aether.sys.Event
import net.eelemental.aether.lib.types.Color
import net.eelemental.aether.types.Vec2F
import net.eelemental.aether.lib.shader.Canvas
import Global._

object Swirl extends GameState {
  val circleTex = Textures.createCircleSoft(32)
  def event(event: Event): Unit = {
  }

  def update() {
  }

  def paint() {

    val r = Renderer.get
    r.filter = Renderer.Filter.Linear
    r.setTargetTexture(buffer)
    val canvas = Canvas.get
    canvas.begin()
    canvas.clear(0xff000000)
    canvas.color = Color.WHITE

    val count = 4001
    val frac = 1f / count
    val radFrac = MathF.PI * 2 * frac
    val time = System.nanoTime() * 0.000000001f

    canvas.save {
      canvas.translate(buffer.sizeX / 2, buffer.sizeY / 2)
      canvas.rotate(time * 0.03f)
      def circle(x: Float): Vec2F = Vec2F(cos(x), sin(x))
      def ssin(x: Float, min: Float = 0, max: Float = 1): Float = (sin(x) * .5f + .5f) * (max - min) + min
      for (i <- 0 until count) {
        val step = i.toFloat / count
        val radStep = step * 2 * MathF.PI
        canvas.color = Color(Colors.hsl(time * 0.1f + step, ssin(time * 0.42f - radStep * 2), ssin(time * 0.5f + radStep))).alpha(.5f)
        canvas.rotate(radFrac * 2)
        canvas.scale(ssin(time * 0.01f, 0.999f, 1))
        //      canvas.transform.scale(0.999f)
        canvas.save {
          canvas.translate(sin(time * radStep) * 1000, 0)
          //      canvas.fillRect(-4,-4,8,8)
          canvas.scale(ssin(time * 0.17f, 16, 128) / (step + .05f))
          canvas.drawTexture(circleTex, -0.5f, -0.5f)
        }

      }
    }

    canvas.end()
    renderBuffer()
  }
}