package dome

import DomeProjection._
import net.eelemental.aether.lib.projection.RayTrace
import net.eelemental.aether.util.MathI
import net.eelemental.aether.sys.Texture
import net.eelemental.aether.sys.Ref
import net.eelemental.aether.types.Vec3D
import net.eelemental.aether.types.Vec2D
import net.eelemental.aether.core.buffers.ByteBuffer
import net.eelemental.aether.util.MathD._

/**
 * Utilities for creating dome projection mapping.
 */
object DomeProjection {

  def saveMapping(mapping: Texture, file: Ref) {
    val buffer = ByteBuffer()
    mapping.encode(buffer, Texture.FileFormat.PNG, 0)
    buffer.flip
    file.saveByteBuffer(buffer)
  }

  type Writer = (Int, Int, Double, Double) => _

  def uvWriter(data: Array[Byte], offset: Int, stride: Int)(x: Int, y: Int, u: Double, v: Double) = {
    val p = y * stride + x * 4 + offset
    val red = (u * 65536).toInt
    val green = (v * 65536).toInt
    data(p + 0) = red.toByte
    data(p + 1) = (red >> 8).toByte
    data(p + 2) = green.toByte
    data(p + 3) = (green >> 8).toByte
  }

  def uvZigzagWriter(data: Array[Byte], offset: Int, stride: Int)(x: Int, y: Int, u: Double, v: Double) = {
    val p = y * stride + x * 4 + offset
    val uint = (u * 65536).toInt
    val vint = (v * 65536).toInt
    data(p + 0) = (uint >> 8).toByte
    data(p + 1) = (vint >> 8).toByte
    data(p + 2) = (if ((uint & 0x100) == 0) uint else (0x1ff - uint)).toByte
    data(p + 3) = (if ((vint & 0x100) == 0) vint else (0x1ff - vint)).toByte
  }

  def createDirectFisheye(sizeX: Int, sizeY: Int, writer: Writer) {
    val size = MathI.min(sizeX, sizeY)
    val ox = (sizeX - size) / 2
    val oy = (sizeY - size) / 2
    for (y <- 0 until size; x <- 0 until size) {
      val u = y / (size - 1.0)
      val v = x / (size - 1.0)
      writer(ox + x, oy + y, u, v)
    }
  }
}

class DomeProjection(
    dispSizeX: Int,
    dispSizeY: Int,
    projectorY: Double,
    projectorZ: Double,
    tilt: Double,
    skew: Double,
    rotate: Double,
    domeTopX: Double,
    domeTopY: Double,
    domeBottomY: Double,
    domeLeftX: Double,
    domeRightX: Double,
    domeSideY: Double,
    pitchMultiplier: Double,
    zigzag: Boolean) {

  def createHalfDomeMap(side: Float, data: Array[Byte], offset: Int, stride: Int) {
    createHalfDomeMap(side, (if (zigzag) uvZigzagWriter _ else uvWriter _)(data, offset, stride))
  }

  /**
   * Create half-dome projection mapping.
   * UV-projection display mapping into RGBA texture.
   * 16-bit UV values are encoded into RGBA:
   * - R contains U low 8 bits
   * - G contains U high 8 bits
   * - B contains V low 8 bits
   * - A contains V high 8 bits
   */
  def createHalfDomeMap(side: Float, writer: Writer) {
    val pSizeX = dispSizeX
    val pSizeY = dispSizeY

    val pz = -projectorZ
    val yRatio = (domeSideY - domeBottomY) / (domeSideY - domeTopY)
    val yMax = dispSizeY - 1.0

    val ct = solve(tilt * yRatio * (tilt - 1), tilt - pz + tilt * yRatio * pz, pz, tilt < 1)
    val cb = yRatio * ct
    println(s"ct: $ct, cb: $cb")
    val py = cb * (1 - pz) / (1 + tilt * cb)

    val c0 = domeSideY / (domeSideY - domeTopY) * ct
    val cMax = (domeSideY - yMax) / (domeSideY - domeTopY) * ct
    println(s" c0: $c0, cMax: $cMax")

    val disp0 = Vec2D(-tilt * c0 - pz, c0 - py)
    val dispMax = Vec2D(-tilt * cMax - pz, cMax - py)
    println(s" disp0: $disp0, dispMax: $dispMax")

    val s = (1 - skew) / (domeSideY - domeTopY)
    val s0 = skew - s * domeTopY
    val sMax = skew + s * (yMax - domeTopY)
    println(s" s0: $s0, sMax: $sMax")
    val x0 = (2 * domeLeftX / (domeLeftX - domeRightX) - 1)
    val xMax = (2 * (domeRightX - (dispSizeX - 1)) / (domeLeftX - domeRightX) + 1)
    println(s" x0: $x0, xMax: $xMax")

    val projPos = Vec3D(0, py, pz * side)
    println(s"Projection $projPos")

    println(s"    $disp0, $dispMax")
    for (y <- 0 until pSizeY; x <- 0 until pSizeX) {
      val printLog = ((y & 0x1ff) == 0 || y == pSizeY - 1) && ((x & 0x1ff) == 0 || x == pSizeX - 1)
      val projDir = {
        val xf = x / (pSizeX - 1f)
        val yf = y / (pSizeY - 1f)
        //        val xSkew = (yf-domeSideY/yMax)/(domeSideY-domeTopY)*skew TODO
        val vx = (x0 * (1 - xf) + xMax * xf) * (s0 * (1-yf) + sMax * yf)
        val vy = disp0.y * (1 - yf) + dispMax.y * yf
        val vz = disp0.x * (1 - yf) + dispMax.x * yf
        (Vec3D(vx, vy, vz) * Vec3D(side, 1, side)).rotateZ(rotate/180*PI)
      }
      if (printLog) println(s"Project dir $x, $y -> $projDir")

      val d = RayTrace.lineSphere3d(projPos, projDir, Vec3D.zero, 1, true)
      val (u, v) = if (d.isNaN()) {
        (0.0, 0.0) // misses dome sphere
      } else {
        val pos = (projPos + projDir * d).normalized
        val quadSeg = pos.z >= 0 && pos.y >= 0
        if (pos.y < 0) (0.0, 0.0) // below dome
        else {
          val closer = pos.z * side < 0 // closer quadrant
          val yaw = atan(pos.x, pos.z)
          val pitch = atan(Vec2D(pos.x, pos.z).length, pos.y)
          val len = pitch / PI * 2 * pitchMultiplier
          if (printLog) println(s"Project dx, dy, $projDir => $d, pos, ${pos.length}, $yaw, $len")
          val u = sin(yaw) * len / 2 + .5
          val v = cos(yaw) * len / 2 + .5
          (u - 0.5 / 256, v - 0.5 / 256.0)
          //                    (u, v)
        }
      }
      writer(x, y, u, v)
    }

  }

  def solve(a: Double, b: Double, c: Double, takeMax: Boolean): Double = {
    val d = sqrt(b * b - 4 * a * c)
    val x0 = (-b + d) / (2 * a)
    val x1 = (-b - d) / (2 * a)
    if (takeMax) max(x0, x1) else min(x0, x1)
  }

}

