package dome

object Shaders {

  val projectionVertex2D = """
#version 130

precision highp float;
precision highp int;

attribute vec4 a_position;
attribute highp vec4 a_texCoord;

varying highp vec4 v_texCoord;

void main()
{
	gl_Position = a_position;
	v_texCoord = a_texCoord;
}
"""
  
  val projectionFragment2D = """
#version 130

precision highp float;
precision highp int;

uniform highp sampler2D u_texture0;
uniform highp sampler2D u_texture1;

varying highp vec4 v_texCoord;

void main()
{
  vec4 source = texture2D(u_texture0, v_texCoord.xy);
  //source.ga -= vec2(0.5/256,0.5/256); // fix texture sampling pixel center offset
  source.ga += source.rb/256; // add uv low bits
	vec4 texColor = texture2D(u_texture1, source.ga);
  if (texture2D(u_texture0, v_texCoord.xy)==vec4(0,0,0,0)) discard;
  gl_FragColor = texColor;
}
"""
  
}