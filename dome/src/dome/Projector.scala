package dome

import scala.util.Random
import net.eelemental.aether.lib.shader.Canvas
import net.eelemental.aether.lib.sheets.DynamicConfig
import net.eelemental.aether.sys.Ref
import net.eelemental.aether.sys.Renderer
import net.eelemental.aether.sys.Texture
import net.eelemental.aether.sys.Event
import net.eelemental.aether.lib.types.Color
import net.eelemental.aether.types.Vec2F
import net.eelemental.aether.types.Vec3F
import net.eelemental.aether.util.MathF
import net.eelemental.aether.util.MathF._

object Projector extends GameState {

  val size = 1024

  var calculate = true

  val data = new Array[Int](size * size)
  lazy val buffer = Texture(Texture.Config(sizeX = size, sizeY = size, flags = Texture.Flag.WRITABLE /*, argb = Some(data)*/ ))

  def event(event: Event): Unit = {
  }

  def update() {
//    if (!calculate) {
//    }
//    if (calculate) {
//      calculate = false
//      createUV()
//      buffer.argb(data)
//      //      createRandom()
//    }
  }

  def createUV() {
    for (y <- 0 until size; x <- 0 until size) {
      val c = Color.RGB(x.toFloat / size, y.toFloat / size, 0).argb
      data(y * size + x) = c
    }
  }

  def createRandom() {
    val r = new Random()
    for (y <- 0 until size; x <- 0 until size) {
      val c = Color.RGB(r.nextFloat(), r.nextFloat(), 0).argb
      data(y * size + x) = c
    }
  }

  class Counter(steps: Int) {
    var counter = 0
    def count(): Boolean = {
      counter -= 1
      if (counter < 0) {
        counter = steps
        true
      } else false
    }
  }

  val rand = new Random()

  def paint() {
    val r = Renderer.get
    r.clear(0x000022)
    val canvas = Canvas.get
    canvas.begin()
    canvas.save {
      import Projection._
      val sx = Config.displaySizeX
      val sy = Config.displaySizeY
      if (Config.projectionCombined) {
        canvas.scale(canvas.sizeX.toFloat / sx)
        canvas.drawTexture(disp0, 0, 0, disp0.sizeX, disp0.sizeY)
      } else {
        canvas.scale(canvas.sizeY.toFloat / (sy * 2))
        canvas.drawTexture(disp0, 0, 0, disp0.sizeX, disp0.sizeY)
        canvas.drawTexture(disp1, 0, disp1.sizeY, disp1.sizeX, disp1.sizeY)
      }
    }
    //    canvas.drawTexture(buffer, 0, 0, size / 4, size / 4)

    canvas.end()

  }

}