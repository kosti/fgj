package dome

import net.eelemental.aether.lib.shader.Canvas
import net.eelemental.aether.lib.sheets.DynamicConfig

import net.eelemental.aether.sys.Ref
import net.eelemental.aether.sys.Renderer
import net.eelemental.aether.sys.Texture
import net.eelemental.aether.core.buffers.ByteBuffer
import net.eelemental.aether.lib.types.Color
import net.eelemental.aether.types.Vec2F
import net.eelemental.aether.types.Vec3F
import net.eelemental.aether.util.MathF
import net.eelemental.aether.util.MathF._
import scala.util.Random

/**
 * Add:
 *
 * Projector positions:
 * Pos: [1034.0, 98.0]
 * Pos: [1023.0, 540.0]
 * Pos: [1032.0, 1005.0]
 * left center bottom: Pos: [111.0, 931.0]
 * right center bottom: Pos: [1944.0, 930.0]
 */
object Projection {

  var dispData: Array[Byte] = _
  var disp0: Texture = _
  var disp1: Texture = _

  def createMapping() {
//    val pSizeX: Int = Config.displaySizeX
//    val pSizeY: Int = Config.displaySizeY
    val mSizeX: Int = Config.mappingSizeX
    val mSizeY: Int = Config.mappingSizeY
    val combined: Boolean = Config.projectionCombined
    val vertical: Boolean = Config.projectionVertical

    //    // clean-up
    //    if (disp0 != null) disp0.release()
    //    if (disp1 != null) disp1.release()
    //    disp0 = null
    //    disp1 = null

    if (disp0 == null) {
      // create structures
      if (!combined) {
        dispData = new Array[Byte](mSizeX * mSizeY * 4)
        disp0 = Texture(Texture.Config(sizeX = mSizeX, sizeY = mSizeY, flags = Texture.Flag.WRITABLE, format = Texture.Format.RGBA_8888))
        disp1 = Texture(Texture.Config(sizeX = mSizeX, sizeY = mSizeY, flags = Texture.Flag.WRITABLE, format = Texture.Format.RGBA_8888))
      } else if (vertical) {
        dispData = new Array[Byte](mSizeX * mSizeY * 8)
        disp0 = Texture(Texture.Config(sizeX = mSizeX, sizeY = mSizeY * 2, flags = Texture.Flag.WRITABLE, format = Texture.Format.RGBA_8888))
      } else {
        dispData = new Array[Byte](mSizeX * mSizeY * 8)
        disp0 = Texture(Texture.Config(sizeX = mSizeX * 2, sizeY = mSizeY, flags = Texture.Flag.WRITABLE, format = Texture.Format.RGBA_8888))
      }
    }

    // create mapping textures
    import Config._
    val dome1 = new DomeProjection(
      mSizeX, mSizeY,
      Dome1.projectorY, Dome1.projectorZ, Dome1.tilt, Dome1.skew, Dome1.rotate, 
      Dome1.topX, Dome1.topY, Dome1.bottomY, Dome1.leftX, Dome1.rightX, Dome1.sideY,
      pitchMultiplier, projectionZigzag)

    val dome2 = new DomeProjection(
      mSizeX, mSizeY,
      Dome2.projectorY, Dome2.projectorZ, Dome2.tilt, Dome2.skew, Dome2.rotate, 
      Dome2.topX, Dome2.topY, Dome2.bottomY, Dome2.leftX, Dome2.rightX, Dome2.sideY,
      pitchMultiplier, projectionZigzag)

    if (!combined) {
      dome1.createHalfDomeMap(1, dispData, 0, mSizeX * 4)
      disp0.write(dispData)
      dome2.createHalfDomeMap(-1, dispData, 0, mSizeX * 4)
      disp1.write(dispData)
    } else if (vertical) {
      dome1.createHalfDomeMap(1, dispData, 0, mSizeX * 4)
      dome2.createHalfDomeMap(-1, dispData, mSizeY * mSizeX * 4, mSizeX * 4)
      disp0.write(dispData)
    } else {
      dome1.createHalfDomeMap(1, dispData, 0, mSizeX * 8)
      dome2.createHalfDomeMap(-1, dispData, mSizeX * 4, mSizeX * 8)
      disp0.write(dispData)
    }
  }

  def createFisheye() {
    val sizeX = 1600
    val sizeY = 1200
    val data = new Array[Byte](sizeX * sizeY * 4)
    DomeProjection.createDirectFisheye(sizeX, sizeY, DomeProjection.uvWriter(data, 0, sizeX*4) _)
    val tex = Texture(Texture.Config(sizeX = sizeX, sizeY = sizeY, flags = Texture.Flag.WRITABLE, format = Texture.Format.RGBA_8888))
    tex.write(data)
    val file: String = Config.projectionFile
    DomeProjection.saveMapping(tex, Ref(file+"-direct.png"))
    tex.release()
  }

  def write() {
    val file: String = Config.projectionFile
    if (Config.projectionCombined) {
      DomeProjection.saveMapping(disp0, Ref(file + ".png"))
    } else {
      DomeProjection.saveMapping(disp0, Ref(file + "-front.png"))
      DomeProjection.saveMapping(disp1, Ref(file + "-back.png"))

    }
  }

}