package dome

import net.eelemental.aether.sys.Renderer
import net.eelemental.aether.lib.shader.Canvas
import Global._
import net.eelemental.aether.sys.Event

object Firefly extends GameState {

  def event(event: Event): Unit = {
  }

  def update() {
  }

  def paint() {
    val r = Renderer.get
    r.setTargetTexture(buffer)
    val canvas = Canvas.get
    canvas.stateTransform.setIdentity()
    canvas.begin()
    canvas.drawTexture(skybox(0), 0, 0, defaultSize, defaultSize)
    canvas.end()

    renderBuffer()

  }

}