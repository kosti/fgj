package dome

import net.eelemental.aether.lib.shader.Canvas
import net.eelemental.aether.util.MathI
import net.eelemental.aether.sys.Event
import net.eelemental.aether.sys.Renderer

import Global._
import net.eelemental.aether.sys.ShaderProgram
import net.eelemental.aether.lib.model.Mesh
import net.eelemental.aether.sys.Ref
import net.eelemental.aether.sys.Texture

object ShaderToy extends GameState {

  lazy val program = ShaderProgram.create(Shaders.projectionVertex2D, fragShader)

  lazy val mesh: Mesh = getMesh()

  lazy val nyan = Texture.load(Ref("res/images/nyan.png")).get
  lazy val logo = Texture.load(Ref("res/images/logo-white.png")).get
  
  def getMesh() = {
    val sizeX = 1 //Global.defaultSize
    val sizeY = 1 //Global.defaultSize
    val mesh = Mesh.staticPT(4)
    mesh.positions.put2F(-1, 1)
    mesh.positions.put2F(1, 1)
    mesh.positions.put2F(-1, -1)
    mesh.positions.put2F(1, -1)
    mesh.texCoords.put2F(0, sizeY)
    mesh.texCoords.put2F(sizeX, sizeY)
    mesh.texCoords.put2F(0, 0)
    mesh.texCoords.put2F(sizeX, 0)
    mesh
  }

  def event(event: Event): Unit = {
  }

  def update() {
  }

  val start = System.currentTimeMillis()

  def paint() {
    //    val r = Renderer.get
    //    r.setTargetTexture(buffer)
    //    val canvas = Canvas.get.asInstanceOf[ShaderCanvas]
    //    canvas.begin()
    //    canvas.stateTransform.setIdentity()
    //    canvas.clear(0xff002200)
    //    val size = MathI.min(buffer.sizeX, buffer.sizeY)
    //    canvas.translate(buffer.sizeX / 2, buffer.sizeY / 2)
    //    canvas.scale(size / 2)
    render()
    //    canvas.end()
    //    renderBuffer()

  }

  def render() {
    val r = Renderer.get
    r.setTargetTexture(buffer)
    r.clear(0)
    r.filter = Renderer.Filter.Linear
    //    program.attributeBuffer(mesh.aPosition, mesh.positions)
    mesh.toProgram(program)

    program.textureUnit(0, logo)
    program.textureUnit(1, nyan)
    program.uniform("iChannel0").get.putI(0)
    program.uniform("iChannel1").get.putI(1)
    //    projectionShader.textureUnit(0, map)
    //    projectionShader.textureUnit(1, buffer)
    program.uniform("iResolution").get.put3F(buffer.sizeX, buffer.sizeY, 0)
    program.uniform("iGlobalTime").get.putF((System.currentTimeMillis() - start) * 0.001f)
    program.draw(ShaderProgram.Mode.TriangleStrip, 0, 4)
    renderBuffer()
  }

  val shadertoyHeader = """
#version 130

uniform vec3 iResolution;
uniform float iGlobalTime;
//uniform float iChannelTime[4];
uniform vec4 iMouse;
//uniform vec4 iDate;
//uniform float iSampleRate;
//uniform vec3 iChannelResolution[4];
uniform sampler2D iChannel0;
uniform sampler2D iChannel1;

varying highp vec4 v_texCoord;

"""

  val shadertoyFooter = """
void main()
{
//  vec4 p;
  vec2 p = vec2(v_texCoord.x, 1-v_texCoord.y)*vec2(2048,2048);
  mainImage(gl_FragColor, p);
//  gl_FragColor = vec4(1,1,1,1);
//  gl_FragColor = p;
//  gl_FragColor.b = v_texCoord.x;
}
"""

  val fragShader = shadertoyHeader + """
// "Fractal Cartoon" - former "DE edge detection" by Kali

// Cartoon-like effect using eiffies's edge detection found here: 
// https://www.shadertoy.com/view/4ss3WB
// I used my own method previously but was too complicated and not compiling everywhere.
// Thanks to the suggestion by WouterVanNifterick. 

// There are no lights and no AO, only color by normals and dark edges.

// update: Nyan Cat cameo, thanks to code from mu6k: https://www.shadertoy.com/view/4dXGWH


//#define SHOWONLYEDGES
#define NYAN 
#define WAVES
//#define BORDER

#define RAY_STEPS 150

#define BRIGHTNESS 1.2
#define GAMMA 1.4
#define SATURATION .65

#define M_PI 3.1415926535897932384626433832795

#define detail .001
#define t iGlobalTime*.2 // .5


const vec3 origin=vec3(-1.,.7,0.);
float det=0.0;


// 2D rotation function
mat2 rot(float a) {
  return mat2(cos(a),sin(a),-sin(a),cos(a));  
}

// "Amazing Surface" fractal
vec4 formula(vec4 p) {
    p.xz = abs(p.xz+1.)-abs(p.xz-1.)-p.xz;
    p.y-=.25;
    p.xy*=rot(radians(35.));
    p=p*2./clamp(dot(p.xyz,p.xyz),.2,1.);
  return p;
}

// Distance function
float de(vec3 pos) {
#ifdef WAVES
  pos.y+=sin(pos.z-t*6.)*.15; //waves!
#endif
  float hid=0.;
  vec3 tpos=pos;
  tpos.z=abs(3.-mod(tpos.z,6.));
  vec4 p=vec4(tpos,1.);
  for (int i=0; i<4; i++) {p=formula(p);}
  float fr=(length(max(vec2(0.),p.yz-1.5))-1.)/p.w;
  float ro=max(abs(pos.x+1.)-.3,pos.y-.35);
      ro=max(ro,-max(abs(pos.x+1.)-.1,pos.y-.5));
  pos.z=abs(.25-mod(pos.z,.5));
      ro=max(ro,-max(abs(pos.z)-.2,pos.y-.3));
      ro=max(ro,-max(abs(pos.z)-.01,-pos.y+.32));
  float d=min(fr,ro);
  return d;
}


// Camera path
vec3 path(float ti) {
  ti*=1.5;
  vec3  p=vec3(sin(ti),(1.-sin(ti*2.))*.5,-ti*5.)*.5;
  return p;
}

// Calc normals, and here is edge detection, set to variable "edge"

float edge=0.;
vec3 normal(vec3 p) { 
  vec3 e = vec3(0.0,det*5.,0.0);

  float d1=de(p-e.yxx),d2=de(p+e.yxx);
  float d3=de(p-e.xyx),d4=de(p+e.xyx);
  float d5=de(p-e.xxy),d6=de(p+e.xxy);
  float d=de(p);
  edge=abs(d-0.5*(d2+d1))+abs(d-0.5*(d4+d3))+abs(d-0.5*(d6+d5));//edge finder
  edge=min(1.,pow(edge,.55)*15.);
  return normalize(vec3(d1-d2,d3-d4,d5-d6));
}


// Used Nyan Cat code by mu6k, with some mods

vec4 rainbow(vec2 p)
{
  float q = max(p.x,-0.1);
  float s = sin(p.x*7.0+t*70.0)*0.08;
  p.y+=s;
  p.y*=1.1;
  
  vec4 c;
  if (p.x>0.0) c=vec4(0,0,0,0); else
  if (0.0/6.0<p.y&&p.y<1.0/6.0) c= vec4(255,43,14,255)/255.0; else
  if (1.0/6.0<p.y&&p.y<2.0/6.0) c= vec4(255,168,6,255)/255.0; else
  if (2.0/6.0<p.y&&p.y<3.0/6.0) c= vec4(255,244,0,255)/255.0; else
  if (3.0/6.0<p.y&&p.y<4.0/6.0) c= vec4(51,234,5,255)/255.0; else
  if (4.0/6.0<p.y&&p.y<5.0/6.0) c= vec4(8,163,255,255)/255.0; else
  if (5.0/6.0<p.y&&p.y<6.0/6.0) c= vec4(122,85,255,255)/255.0; else
  if (abs(p.y)-.05<0.0001) c=vec4(0.,0.,0.,1.); else
  if (abs(p.y-1.)-.05<0.0001) c=vec4(0.,0.,0.,1.); else
    c=vec4(0,0,0,0);
  c.a*=.8-min(.8,abs(p.x*.08));
  c.xyz=mix(c.xyz,vec3(length(c.xyz)),.15);
  return c;
}

vec4 nyan(vec2 p)
{
  vec2 uv = p*vec2(0.4,1.0);
  float ns=3.0;
  float nt = iGlobalTime*ns; nt-=mod(nt,240.0/256.0/6.0); nt = mod(nt,240.0/256.0);
  float ny = mod(iGlobalTime*ns,1.0); ny-=mod(ny,0.75); ny*=-0.05;
  vec4 color = texture2D(iChannel1,vec2(uv.x/3.0+210.0/256.0-nt+0.05,.5-uv.y-ny));
  if (uv.x<-0.3) color.a = 0.0;
  if (uv.x>0.2) color.a=0.0;
  return color;
}

vec4 logoTex(vec2 p)
{
  p = p * vec2(1.,1.8);
  vec2 uv = (p+vec2(1))/vec2(-2.);
  vec4 color = texture2D(iChannel0,uv);
  if (p.x<-1. || p.x>1.) color.a = 0.0;
  if (p.y<-1. || p.y>1.) color.a = 0.0;
  return color;
}


// Raymarching and 2D graphics

vec3 raymarch(in vec3 from, in vec3 dir) 

{
  edge=0.;
  vec3 p, norm;
  float d=100.;
  float totdist=0.;
  for (int i=0; i<RAY_STEPS; i++) {
    if (d>det && totdist<25.0) {
      p=from+totdist*dir;
      d=de(p);
      det=detail*exp(.13*totdist);
      totdist+=d; 
    }
  }
  vec3 col=vec3(0.);
  p-=(det-d)*dir;
  norm=normal(p);
#ifdef SHOWONLYEDGES
  col=1.-vec3(edge); // show wireframe version
#else
  col=(1.-abs(norm))*max(0.,1.-edge*.8); // set normal as color with dark edges
#endif    
  totdist=clamp(totdist,0.,26.);
  dir.y-=.02;
  float sunsize=6.; //7.-max(0.,texture2D(iChannel0,vec2(.6,.2)).x)*5.; // responsive sun size
  float an=atan(dir.x,dir.y)+iGlobalTime*1.5; // angle for drawing and rotating sun
  float s=pow(clamp(1.0-length(dir.xy)*sunsize-abs(.2-mod(an,.4)),0.,1.),.1); // sun
  float sb=pow(clamp(1.0-length(dir.xy)*(sunsize-.2)-abs(.2-mod(an,.4)),0.,1.),.1); // sun border
  float sg=pow(clamp(1.0-length(dir.xy)*(sunsize-4.5)-.5*abs(.2-mod(an,.4)),0.,1.),3.); // sun rays
  float y=mix(.45,1.2,pow(smoothstep(0.,1.,.75-dir.y),2.))*(1.-sb*.5); // gradient sky
  
  // set up background with sky and sun
  vec3 backg=vec3(0.5,0.,1.)*((1.-s)*(1.-sg)*y+(1.-sb)*sg*vec3(1.,.8,0.15)*3.);
     backg+=vec3(1.,.9,.1)*s;
     backg=max(backg,sg*vec3(1.,.9,.5));
  
  col=mix(vec3(1.,.9,.3),col,exp(-.004*totdist*totdist));// distant fading to sun color
  if (totdist>25.) col=backg; // hit background
  col=pow(col,vec3(GAMMA))*BRIGHTNESS;
  col=mix(vec3(length(col)),col,SATURATION);

  vec4 logo=logoTex(dir.xy*4.-vec2(0,1.3));
  if (totdist>8. && dir.z<0) col=mix(col,max(vec3(.2),logo.xyz),logo.a);    

#ifdef SHOWONLYEDGES
  col=1.-vec3(length(col));
#else
  col*=vec3(1.,.9,.85);
#ifdef NYAN
  dir.yx*=rot(dir.x);
  vec2 ncatpos=(dir.xy+vec2(-3.+mod(-t,6.),-.6));
  vec4 ncat=nyan(ncatpos*5.);
  vec4 rain=rainbow(ncatpos*10.+vec2(.8,.5));
  if (totdist>8.) col=mix(col,max(vec3(.2),rain.xyz),rain.a*.9);
  if (totdist>8.) col=mix(col,max(vec3(.2),ncat.xyz),ncat.a*.9);
#endif
#endif
  return col;
}

// get camera position
vec3 move(inout vec3 dir) {
  vec3 go=path(t);
  vec3 adv=path(t+.7);
  float hd=de(adv);
  vec3 advec=normalize(adv-go);
  float an=adv.x-go.x; an*=min(1.,abs(adv.z-go.z))*sign(adv.z-go.z)*.7;
  dir.xy*=mat2(cos(an),sin(an),-sin(an),cos(an));
    an=advec.y*1.7;
  dir.yz*=mat2(cos(an),sin(an),-sin(an),cos(an));
  an=atan(advec.x,advec.z);
  dir.xz*=mat2(cos(an),sin(an),-sin(an),cos(an));
  return go;
}


void rotateX(inout vec3 v, float rad)
{
    float s = sin(rad);
    float c = cos(rad);
    v = vec3(v.x, c * v.y - s * v.z, s * v.y + c * v.z);

}

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
  vec2 uv = fragCoord.xy / iResolution.xy*2.-1.;
  vec2 oriuv=uv;
  uv.y*=iResolution.y/iResolution.x;
  //vec2 mouse=(iMouse.xy/iResolution.xy-.5)*3.;
  //if (iMouse.z<1.) mouse=vec2(0.,-0.05);

  float fov=.9-max(0.,.7-iGlobalTime*.3);
  //vec3 dir=normalize(vec3(uv*fov,1.));

  float a = atan(uv.y, uv.x);
  float b = length(uv)*1.4;
//  if (b>1) discard;
  b = b* M_PI * .5;
  uv = normalize(uv)*sin(b);
  vec3 dir=normalize(vec3(uv.x,cos(b), -uv.y));
  rotateX(dir, 24./180.*M_PI);

  //dir.yz*=rot(mouse.y);
  //dir.xz*=rot(mouse.x);

  vec3 from=origin+move(dir);
  vec3 color=raymarch(from,dir); 

  #ifdef BORDER
  color=mix(vec3(0.),color,pow(max(0.,.95-length(oriuv*oriuv*oriuv*vec2(1.05,1.1))),.3));
  #endif
  fragColor = vec4(color,1.);
}
""" + shadertoyFooter
}