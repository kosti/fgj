package dome

import net.eelemental.aether.sys.Renderer
import net.eelemental.aether.lib.shader.Canvas

import Global._
import net.eelemental.aether.sys.Event
import net.eelemental.aether.util.MathI
import net.eelemental.aether.lib.util.Textures
import net.eelemental.aether.lib.util.Colors
import net.eelemental.aether.util.MathF._

object Quantum extends GameState {

  val tex = Textures.createCircleSoft(16)

  def event(event: Event): Unit = {
  }

  def update() {
  }

  def paint() {
    val r = Renderer.get
    r.setTargetTexture(buffer)
    val canvas = Canvas.get
    canvas.begin()
    canvas.stateTransform.setIdentity()
    canvas.clear(0xff002200)
    val size = MathI.min(buffer.sizeX, buffer.sizeY)
    canvas.translate(buffer.sizeX / 2, buffer.sizeY / 2)
    canvas.scale(size / 2)
    render(canvas)
    canvas.end()
    renderBuffer()

  }

  val start = System.currentTimeMillis()
  def render(canvas: Canvas) {
    canvas.color = 0xf444400
    canvas.fillCircle(0, 0, 1)
    canvas.color = 0xffffffff

    val time = (System.currentTimeMillis() - start) * 0.01f

    def color(r: Float, g: Float, b: Float) = Colors.argb(1, (r + 1) / 2, (g + 1) / 2, (b + 1) / 2)
    canvas.translate(1, 0)
    var s = 1f
    for (i <- 0 until 100) {
      canvas.color = color(sin(i * 0.1f + time), sin(i * 0.2f + time * 0.3f), sin(i * 0.5f * 0.002f + time))
      canvas.save {
        //        canvas.translate(sin(i*0.01f+time*0.003f), sin(i*0.02f+time*0.021f))
        //        canvas.translate(sin(i*0.01f+time*0.003f), sin(i*0.02f+time*0.021f))
        canvas.rotate(sin(time * 0.043f))
        canvas.scale(s)
        canvas.translate(1, 0)
        canvas.drawTexture(tex, -1, -1, 2, 2)
        s*=0.95f
      }
    }

  }

}