package dome

import net.eelemental.aether.util.MathF
import net.eelemental.aether.lib.shader.Canvas
import net.eelemental.aether.types.Mat2F
import net.eelemental.aether.sys.ShaderProgram
import net.eelemental.aether.types.Vec2F
import net.eelemental.aether.sys.Renderer
import net.eelemental.aether.lib.about.Logo

import scala.util.Random
import Global._
import net.eelemental.aether.types.Mat3F
import net.eelemental.aether.sys.Event
import net.eelemental.aether.util.MathI
import net.eelemental.aether.sys.KeyEvent
import net.eelemental.aether.sys.PointerEvent
import net.eelemental.aether.sys.MouseEvent

object Jams extends GameState {

  val start = System.currentTimeMillis() - 1000000

  val rand = new Random()
  var background = 0

  class Roller() {
    val size = rand.nextFloat() * .8f
    val speed = rand.nextFloat() * 30 + 2
    val color = 0xff000000 | rand.nextInt()
    val image = rand.nextInt(5)
  }

  val rollers = for (i <- 0 until 20) yield new Roller()

  var paintRollers = false
  var paintMarkers = true

  def event(event: Event): Unit = {
    event match {
      case KeyEvent(Event.Type.KeyPress, keyCode, _, _) =>
        keyCode match {
          case '2'                  => paintRollers = !paintRollers
          case '3'                  => paintMarkers = !paintMarkers
          case Event.KeyCode.RIGHT => background = MathI.mod((background + 1) , (Global.images.size + 1))
          case Event.KeyCode.LEFT  => background = MathI.mod((background - 1) , (Global.images.size + 1))
          case _                    =>
        }
      case MouseEvent(Event.Type.PointerPress, pos, _) =>
        println("Pos: " + pos)
      case _ =>
    }
  }
  def update() {

  }

  def paint() {
    val r = Renderer.get
    //    r.setTa

    r.clear(0)
    r.setTargetTexture(buffer)
    //    r.clear(0xff444400)
    val canvas = Canvas.get
    canvas.begin()
    canvas.clear(0x004400)
    canvas.color = 0xffffffff
    canvas.save {
      background match {
        case 0 => canvas.save {
          canvas.scale(buffer.sizeX / 2, buffer.sizeY / 2)
          canvas.translate(1, 1)
          canvas.color = 0xff000044
          canvas.fillCircle(0, 0, 1)
          canvas.color = 0xffffffff
          for (r <- 0 until 18) {
            val cos = MathF.cos(r * 10f / 180 * MathF.PI)
            val sin = MathF.sin(r * 10f / 180 * MathF.PI)
            canvas.drawLine(cos, sin, -cos, -sin)
          }
          for (r <- 1 to 9) {
            canvas.drawCircle(0, 0, r / 9f)
          }
        }
        case _ => canvas.drawTexture(Global.images(background - 1), 0, 0, buffer.sizeX, buffer.sizeY)
      }
      if (paintRollers) {
        val time = (System.currentTimeMillis() - start).toFloat * .1f
        for (roller <- rollers) {
          canvas.color = roller.color
          val t = time * roller.speed * 0.00001f
          val v = Vec2F(MathF.cos(t), MathF.sin(t))
          val p = v * (1 - roller.size)
          //      canvas.fillCircle(p.x, p.y, roller.radius)
          canvas.save {
            val tx = Mat3F(v.y, -v.x, 0, v.x, v.y, 0, 0, 0, 1)
            canvas.stateTransform.matrix *= tx
            canvas.translate(-roller.size / 2, (1 - roller.size - 0.01f))
            canvas.scale(roller.size)
            canvas.drawTexture(texture(roller.image), 0, 0)
          }
        }
      }

      Logo.paint()
    }

    canvas.end()

    //    canvas.drawTexture(projection1, 0, 0, 256, 256)

    renderBuffer()

  }

}
