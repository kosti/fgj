package dome

import net.eelemental.aether.sys.Event

abstract class GameState {

  def event(event: Event)
  def update()
  def paint()

}