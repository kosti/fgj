package dome

import net.eelemental.aether.sys.Texture
import net.eelemental.aether.sys.Ref
import net.eelemental.aether.lib.model.Mesh
import net.eelemental.aether.sys.ShaderProgram
import net.eelemental.aether.sys.Renderer
import net.eelemental.aether.lib.shader.Canvas
import net.eelemental.aether.lib.sheets.DynamicConfig
import net.eelemental.aether.util.MathI

object Global {
  var enableProjection = true
  var paintMarkers = true
  var projectionMap: Int = 0
  var projectionMapCount: Int = 2
  var uniformScale = false
  val configSheet = new DynamicConfig(Ref("config.csv"), Config.values)

  lazy val texture = for (i <- 1 to 5) yield Texture.load(Ref(s"res/images/ukkeli$i.png")).get
  lazy val skybox = for (i <- 0 until 4) yield Texture.load(Ref(s"res/skybox/skybox$i.jpg")).get

  val defaultSize = 2048
  lazy val buffer = Texture(0, defaultSize, defaultSize)

  lazy val projection = {
    //    Texture(Texture.Config(format = Texture.Format.RG_16, file = Some(Ref("res/projection/UV-blurred.png"))))
    //    Texture(Ref("res/projection/UV-new-small.png"))
    Projection.disp0
    //        Texture(Ref("res/projection/projection0.png"))
  }

  lazy val images = IndexedSeq(
    Texture.load(Ref("res/projection/TestGrid-2048.png")).get,
    Texture.load(Ref("res/images/newhorizons.jpg")).get,
    Texture.load(Ref("res/images/mosriver.jpg")).get,
    Texture.load(Ref("res/images/tree.jpg")).get,
    Texture.load(Ref("res/images/mirror-infinite.jpg")).get)

  lazy val projectionShader = ShaderProgram.create(Shaders.projectionVertex2D, Shaders.projectionFragment2D)
  lazy val projectionMesh: Mesh = {
    val mesh = Mesh.staticPT(4)
    mesh.positions.put2F(-1, -1)
    mesh.positions.put2F(1, -1)
    mesh.positions.put2F(-1, 1)
    mesh.positions.put2F(1, 1)
    mesh.texCoords.put2F(0, 1)
    mesh.texCoords.put2F(1, 1)
    mesh.texCoords.put2F(0, 0)
    mesh.texCoords.put2F(1, 0)
    mesh
  }

  def renderBuffer() {
    val r = Renderer.get
    r.setTargetDisplay()
    val canvas = Canvas.get
    canvas.scale(buffer.sizeX / 2, buffer.sizeY / 2)
    canvas.translate(1, 1)
    canvas.clear(0)
    canvas.color = 0xffffffff
    if (enableProjection) {
      r.filter = Renderer.Filter.Linear
      projectionMesh.toProgram(projectionShader)
      val map = projectionMap match {
        case 0 => Projection.disp0
        case 1 => Projection.disp1
      }
      projectionShader.textureUnit(0, map)
      projectionShader.textureUnit(1, buffer)
      projectionShader.uniform("u_texture0").get.putI(0)
      projectionShader.uniform("u_texture1").get.putI(1)
      projectionShader.draw(ShaderProgram.Mode.TriangleStrip, 0, 4)
      if (paintMarkers) {
        import Config._
        val sx = canvas.sizeX.toFloat / mappingSizeX
        val sy = canvas.sizeY.toFloat / mappingSizeY
        canvas.begin()
        canvas.save {
          canvas.scale(sx, sy)
          val map = projectionMap match {
            case 0 => drawMarkers(canvas, Dome1.leftX, Dome1.rightX, Dome1.topX, Dome1.topY, Dome1.sideY, Dome1.bottomY)
            case 1 => drawMarkers(canvas, Dome2.leftX, Dome2.rightX, Dome2.topX, Dome2.topY, Dome2.sideY, Dome2.bottomY)
            case _ =>
          }
        }
        canvas.end()
      }

    } else {
      canvas.begin()
      canvas.clear(0)
      if (uniformScale) {
        canvas.save {
          import Config.Sphere._
          canvas.translate(canvas.sizeX / 2 + offsetX, canvas.sizeY / 2 + offsetY)
          val size = MathI.min(canvas.sizeX, canvas.sizeY)
          canvas.scale(size / 2 * scaleX, size / 2 * scaleY)
          canvas.drawTexture(buffer, -1, -1, 2, 2)
        }
      } else {
        canvas.drawTexture(buffer, 0, 0, canvas.sizeX, canvas.sizeY)
      }
      canvas.end()
    }

  }

  def drawMarkers(canvas: Canvas, left: Float, right: Float, topX: Float, topY: Float, sideY: Float, bottomY: Float) {
    val centerX = (left + right) / 2
    val radius = 2
    canvas.color = 0xff88ff88
    canvas.drawCircle(left, sideY, radius)
    canvas.drawCircle(right, sideY, radius)
    canvas.drawCircle(centerX, bottomY, radius)
    canvas.drawCircle(topX, topY, radius)
    canvas.drawCircle(centerX, topY, radius)
  }
}
